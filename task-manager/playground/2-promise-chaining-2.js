require("../src/db/mongoose");
const Task = require("../src/models/task");

Task.findByIdAndDelete("5c9790e6e6020f5716146d71")
  .then(task => {
    console.log(task);
    return Task.countDocuments({ completed: false });
  })
  .then(result => {
    console.log(result);
  })
  .catch(e => {
    console.log(e);
  });
