const jwt = require("jsonwebtoken");

// try to decode token at www.base64decode.org

const myFunction = async () => {
  // create token
  const token = jwt.sign({ _id: "abcd123" }, "thisismynewcourse", {
    expiresIn: "7 days"
  });
  console.log(token);
  // verify token
  const data = jwt.verify(token, "thisismynewcourse");
  console.log(data);
};

myFunction();
