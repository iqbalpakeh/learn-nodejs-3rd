const Task = require("../src/models/task");
const User = require("../src/models/user");

// NOTE!!!!!!! THIS CODE IS NOT WORKING, JUST EXAMPLE

const main = async () => {
  // to fetch owner from task
  const task = await Task.findById("5c9fdb59007844113f8bdfe5");
  await task.populate("owner").execPopulate();
  console.log("owner", task.owner);

  // to fetch task from owner
  const user = await User.findById("5c9fdb59007844113f8bdfe5");
  await user.populate("tasks").execPopulate();
  console.log(user.tasks);
};

console.log("Test");
main();
